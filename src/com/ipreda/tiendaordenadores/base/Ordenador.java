package com.ipreda.tiendaordenadores.base;

import java.time.LocalDate;

public abstract class Ordenador {
    /**
     * Atributos que definen la clase clase ordenador.
     */
    private String marca;
    private String modelo;
    private String procesador;
    private double ghzProcesador;
    private String memoriaRam;
    private String tarjetaGrafica;
    private String numSerie;
    private float precio;
    private String so;
    private LocalDate fechaAlta;

    /**
     * Constructor de la clase Ordenador.
     *
     * @param marca          MARCA DEL ORDENADOR
     * @param modelo         MODELO DEL ORDENADOR
     * @param procesador     PROCESADOR DEL ORDENADOR
     * @param memoriaRam     MEMORIA RAM DEL ORDENADOR
     * @param ghzProcesador  VELOCIDAD DEL PROCESAODR
     * @param tarjetaGrafica TARJETA GRAFICA DEL ORDENADOR
     * @param numSerie       NUMERO DE SERIE DEL ORDENADOR
     * @param precio         PRECIO TOTAL DEL ORDENADOR
     * @param so             SISTEMA OPERATIVO QUE INCORPORA EL ORDENADOR
     * @param fechaAlta      FECHA DE ALTA DEL ORDENADOR
     */
    public Ordenador(String marca, String modelo, String procesador, double ghzProcesador, String memoriaRam, String tarjetaGrafica, String numSerie,float precio, String so, LocalDate fechaAlta) {
        this.marca = marca;
        this.modelo = modelo;
        this.procesador = procesador;
        this.ghzProcesador = ghzProcesador;
        this.memoriaRam = memoriaRam;
        this.tarjetaGrafica = tarjetaGrafica;
        this.numSerie = numSerie;
        this.precio = precio;
        this.so = so;
        this.fechaAlta = fechaAlta;
    }

    /**
     * Constructor vacío del ordenador
     */
    public Ordenador() {

    }

    /**
     * Getter de marca.
     *
     * @return devuelve el valor que hay en el parametro marca.
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Setter de marca.
     *
     * @param marca sirve para introducir datos en el parametro marca.
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * Getter de modelo
     *
     * @return devuelve el valor que hay en el parametro modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * Setter de modelo
     *
     * @param modelo sirve para introducir datos en el parametro modelo
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * Getter de procesador
     *
     * @return devuelve el valor que hay en el parametro procesador
     */
    public String getProcesador() {
        return procesador;
    }

    /**
     * Setter de procesador
     *
     * @param procesador usado para introducir datos en el parametro procesador
     */
    public void setProcesador(String procesador) {
        this.procesador = procesador;
    }

    /**
     * Getter de GHZProcesador
     *
     * @return devuelve el valor que hay en el parametro GHzProcesador
     */
    public double getGhzProcesador() {
        return ghzProcesador;
    }

    /**
     * Setter de GHzProcesador
     *
     * @param ghzProcesador usado para introducir datos en el parametro.
     */
    public void setGhzProcesador(double ghzProcesador) {
        this.ghzProcesador = ghzProcesador;
    }

    /**
     * Getter de tarjetaGrafica
     *
     * @return devuelve el valor que hay en el parametro de tarjeta grafica
     */
    public String getTarjetaGrafica() {
        return tarjetaGrafica;
    }

    /**
     * Setter de tarjeta grafica
     *
     * @param tarjetaGrafica usado para introducir datos en el parametro
     */
    public void setTarjetaGrafica(String tarjetaGrafica) {
        this.tarjetaGrafica = tarjetaGrafica;
    }

    /**
     * Getter de precio
     *
     * @return devuelve el valor que hay en el parametro
     */
    public float getPrecio() {
        return precio;
    }

    /**
     * Setter de precio
     *
     * @param precio usado para la introduccion de datos en el parametro
     */
    public void setPrecio(float precio) {
        this.precio = precio;
    }

    /**
     * Getter de fecha alta
     *
     * @return devuelve el valor del parametro
     */
    public LocalDate getFechaAlta() {
        return fechaAlta;
    }

    /**
     * Setter de fecha alta
     *
     * @param fechaAlta usado para introducir datos en el parametro
     */
    public void setFechaAlta(LocalDate fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    /**
     * Getter de SO.
     *
     * @return devuelve los datos que hay en el parametro
     */
    public String getSo() {
        return so;
    }

    /**
     * Setter de S.O.
     *
     * @param so usado para introducir datos en el parametro
     */
    public void setSo(String so) {
        this.so = so;
    }

    /**
     * Getter de Memoria RAM
     *
     * @return devuelve el valor almacenado en el parametro
     */
    public String getMemoriaRam() {
        return memoriaRam;
    }

    /**
     * Setter de memoria ram
     *
     * @param memoriaRam introduce un valor en el parametro
     */
    public void setMemoriaRam(String memoriaRam) {
        this.memoriaRam = memoriaRam;
    }

    /**
     * Getter del numero de serie
     * @return valor almacenado en la variable o atributo
     */
    public String getNumSerie() {
        return numSerie;
    }

    /**
     * Setter de numSerie
     * @param numSerie introduce un valor en la variable.
     */
    public void setNumSerie(String numSerie) {
        this.numSerie = numSerie;
    }
}
