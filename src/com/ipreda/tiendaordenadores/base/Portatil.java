package com.ipreda.tiendaordenadores.base;

import java.time.LocalDate;

public class Portatil extends Ordenador {
    /**
     * Atributo unico de la clase Portatil que hereda de la clase ordenador.
     */
    private String resPantalla;

    /**
     * Constructor vacio de la clase
     */
    public Portatil() {
        super();
    }

    /**
     * Constructor con parametros heredados de la clase Ordenador y el atributo unico de esta clase.
     *
     * @param marca          parametro heredado
     * @param modelo         parametro heredado
     * @param procesador     parametro heredado
     * @param ghzProcesador  parametro heredado
     * @param memoriaRam     parametro heredado
     * @param tarjetaGrafica parametro heredado
     * @param numSerie       parametro heredado
     * @param precio         parametro heredado
     * @param so             parametro heredado
     * @param fechaAlta      parametro heredado
     * @param numSerie       parametro unico de la clase
     * @see Ordenador
     */

    public Portatil(String marca, String modelo, String procesador, double ghzProcesador, String memoriaRam, String tarjetaGrafica, String numSerie, float precio, String so, LocalDate fechaAlta, String resPantalla) {
        super(marca, modelo, procesador, ghzProcesador, memoriaRam, tarjetaGrafica, numSerie, precio, so, fechaAlta);
        this.resPantalla = resPantalla;
    }

    /**
     * Getter del parametro resolucion pantalla
     *
     * @return devuelve el valor almacenado en el parametro
     */
    public String getResPantalla() {
        return resPantalla;
    }

    /**
     * Setter de numero serie
     *
     * @param resPantalla usado para introducir datos en el parametro.
     */
    public void setResPantalla(String resPantalla) {
        this.resPantalla = resPantalla;
    }


    /**
     * Metodo toString de la clase
     *
     * @return devuelve un system.print para poder "pintar" los datos donde sea requerido.
     */
    @Override
    public String toString() {
        return "PC PORTATIL: " + "\n\t" +
                "MARCA: " + getMarca() + "\n\t" +
                "MODELO: " + getModelo() + "\n\t" +
                "RESOLUCION PANTALLA: " + getResPantalla() + "\n\t" +
                "MEMORIA RAM: " + getMemoriaRam() + "\n\t" +
                "PROCESADOR: " + getProcesador() + "\n\t" +
                "GHz PROCESADOR: " + getGhzProcesador() + "\n\t" +
                "TARJETA GRAFICA: " + getTarjetaGrafica() + "\n\t" +
                "PRECIO: " + getPrecio() + "\n\t" +
                "S.O.: " + getSo() + "\n\t" +
                "FECHA ALTA: " + getFechaAlta();

    }
}
