package com.ipreda.tiendaordenadores.base;

import java.time.LocalDate;

public class Torre extends Ordenador {
    /**
     * Atributo unico de la clase "Torre" que hereda campos de la clase ordenador.
     */
    private String placaBase;

    /**
     * Constructor vacio de la clase
     */
    public Torre() {
        super();
    }

    /**
     * Constructor con parametros heredados de la clase padre: ordenador
     *
     * @param marca          parametro heredado
     * @param modelo         parametro heredado
     * @param procesador     parametro heredado
     * @param ghzProcesador  parametro heredado
     * @param memoriaRam     parametro heredado
     * @param tarjetaGrafica parametro heredado
     * @param numSerie       parametro heredado
     * @param precio         parametro heredado
     * @param so             parametro heredado
     * @param fechaAlta      parametro heredado
     * @param placaBase      atributo unico de la clase
     * @see Ordenador
     */

    public Torre(String marca, String modelo, String procesador, double ghzProcesador, String memoriaRam, String tarjetaGrafica, String numSerie, float precio, String so, LocalDate fechaAlta, String placaBase) {
        super(marca, modelo, procesador, ghzProcesador, memoriaRam, tarjetaGrafica, numSerie, precio, so, fechaAlta);
        this.placaBase = placaBase;
    }

    /**
     * Getter de placa base
     *
     * @return devuelve el valor almacenado en placa base
     */
    public String getPlacaBase() {
        return placaBase;
    }

    /**
     * Setter de placa base
     *
     * @param placaBase usado para introducir datos en el parametro
     */
    public void setPlacaBase(String placaBase) {
        this.placaBase = placaBase;
    }

    /**
     * ToString de la clase ordenador.
     *
     * @return devuelve un system.print para poder "pintar" los datos donde sea requerido.
     */
    @Override
    public String toString() {
        return "PC TORRE: " + '\n' +
                "MARCA: " + getMarca() + "\n\t" +
                "MODELO: " + getModelo() + "\n\t" +
                "PROCESADOR: " + getProcesador() + "\n\t" +
                "GHz PROCESADOR: " + getGhzProcesador() + "\n\t" +
                "MEMORIA RAM: " + getMemoriaRam() + "\n\t" +
                "PLACA BASE: " + getPlacaBase() + "\n\t" +
                "TARJETA GRAFICA: " + getTarjetaGrafica() + "\n\t" +
                "PRECIO: " + getPrecio() + "\n\t" +
                "S.O.: " + getSo() + "\n\t" +
                "FECHA ALTA: " + getFechaAlta();


    }
}
