package com.ipreda.tiendaordenadores.util;


import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

public class Util {


    /**
     * Metodo usado para notificar al usuario mediante una ventana de que se ha producido un error.
     *
     * @param mensaje recibe un string de la clase controladora.
     * @see com.ipreda.tiendaordenadores.gui.OrdenadorControlador
     */
    public static void errorMsg(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, "ERROR", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Ventana para confirmar terminar la ejeucion del programa.
     *
     * @param mensaje recibe un string de la clase controladora
     * @param titulo  recibe un titulo de la clase controladora
     * @return devuelve parametros
     * @see com.ipreda.tiendaordenadores.gui.OrdenadorControlador
     */
    public static int mensajeConfirmacion(String mensaje, String titulo) {
        return JOptionPane.showConfirmDialog(null, mensaje, titulo, JOptionPane.YES_NO_OPTION);
    }

    /**
     * Metodo para la creacion de archivos.
     *
     * @param rutaDefecto      recibe la ruta por defecto de la clase controladora
     * @param tipoArchivo      recibe el tipo de archivo a seleccionar de la clase controladora
     * @param extensionArchivo recibe la extension que va a tener el archivo de la calse controladora
     * @return
     * @see com.ipreda.tiendaordenadores.gui.OrdenadorControlador
     */
    public static JFileChooser crearSelectorFicheros(File rutaDefecto, String tipoArchivo, String extensionArchivo) {
        JFileChooser selectorFichero = new JFileChooser();
        if (rutaDefecto != null) {
            selectorFichero.setCurrentDirectory(rutaDefecto);
        }
        if (extensionArchivo != null) {
            FileNameExtensionFilter filtro = new FileNameExtensionFilter(tipoArchivo, extensionArchivo);
            selectorFichero.setFileFilter(filtro);
        }
        return selectorFichero;
    }


}
