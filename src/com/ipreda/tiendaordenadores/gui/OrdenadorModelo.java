package com.ipreda.tiendaordenadores.gui;

import com.ipreda.tiendaordenadores.base.Ordenador;
import com.ipreda.tiendaordenadores.base.Portatil;
import com.ipreda.tiendaordenadores.base.Torre;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class OrdenadorModelo {

    /**
     * Creacion de un arraylist con las variables de la clase Ordenador
     * @see Ordenador
     */
    private ArrayList<Ordenador> listaOrdenadores;


    /**
     * Constructor de la clase OrdenadorModelo
     * @see OrdenadorModelo
      */
    public OrdenadorModelo() {
        listaOrdenadores = new ArrayList<Ordenador>();
    }

    /**
     * Metodo para obtener los datos almacenados en el arraylist
     * @return devuelve los datos almacenados.
     */
    public ArrayList<Ordenador> obtenerOrdenadores() {
        return listaOrdenadores;
    }

    /**
     * Metodo para enviar datos a los parametros o atributos y almacenarlos en las clases correspondientes.
     * @param marca envia y almacena el dato de marca
     * @param modelo envia y elmacena el dato de modelo
     * @param procesador envia y almacena el dato del procesador
     * @param ghzProcesador envia y almacena el dato de ghzprocesador
     * @param memoriaRam envia y almacena el dato de memoriaram
     * @param grafica envia y almacena el dato de grafica
     * @param numSerie envia y almacena el numero de serie
     * @param precio envia y almacena el precio
     * @param so envia y almacena el s.o.
     * @param fechaAlta envia la fecha alta
     * @param placaBase envia el dato de placa base
     */
    public void altaTorre(String marca, String modelo, String procesador, double ghzProcesador, String memoriaRam, String grafica, String numSerie, float precio, String so, LocalDate fechaAlta, String placaBase) {

        Torre nuevaTorre = new Torre(marca, modelo, procesador, ghzProcesador, memoriaRam, grafica, numSerie, precio, so, fechaAlta, placaBase);
        listaOrdenadores.add(nuevaTorre);

    }
    /**
     * Metodo para enviar datos a los parametros o atributos y almacenarlos en las variables en las correspondientes clases.
     * @param marca envia y almacena el dato de marca
     * @param modelo envia y elmacena el dato de modelo
     * @param procesador envia y almacena el dato del procesador
     * @param ghzProcesador envia y almacena el dato de ghzprocesador
     * @param memoriaRam envia y almacena el dato de memoriaram
     * @param grafica envia y almacena el dato de grafica
     * @param numSerie envia y almacena el numero de serie
     * @param precio envia y almacena el precio
     * @param so envia y almacena el s.o.
     * @param fechaAlta envia la fecha alta
     * @param resPantalla envia el dato de resolucion pantalla
     */
    public void altaPortatil(String marca, String modelo, String procesador, double ghzProcesador, String memoriaRam, String grafica, String numSerie, float precio, String so, LocalDate fechaAlta, String resPantalla) {
        Portatil nuevoPortatil = new Portatil(marca, modelo, procesador, ghzProcesador, memoriaRam, grafica, numSerie, precio, so, fechaAlta, resPantalla);
        listaOrdenadores.add(nuevoPortatil);
    }

    /**
     * Metodo para comprobar si ya se ha introducido un valor previo igual.
     * @param numSerie se comprueba mediante el parametro de numeroSerie
     * @return devuelve "True" si ya se ha introducido un valor previo igual al que se quiere introducir.
     * @return devuelve "False" si no se ha introducido un valor previo igual.
     */
    public boolean existeNumSerie(String numSerie) {
        for (Ordenador unOrdenador : listaOrdenadores) {
            if (unOrdenador.getNumSerie().equalsIgnoreCase(numSerie)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Metodo para exportar los datoa a un fichero.
     * @param fichero recibe un parametro de Tipo File.
     * @throws ParserConfigurationException controla la excepcion en caso de haber fallo al parsear.
     * @throws TransformerException controla la excepcion en caso de haber fallo al transformar el fichero.
     */
    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);


        //Creo el nodo raiz, la etiqueta "padre"
        Element raiz = documento.createElement("Ordenador");
        documento.getDocumentElement().appendChild(raiz);

        //Creo dos elementos, nodoOrdenador y nodoDatos. Los inicializo en null.
        Element nodoOrdenador = null, nodoDatos = null;
        Text texto = null;

        for (Ordenador unOrdenador : listaOrdenadores) {
            /*Creo dentro de la etiqueta raiz (Ordenador) otra etiqueta en funcion del tipo de ordenador que se almacene, torre o portatil.*/

            if (unOrdenador instanceof Torre) {
                nodoOrdenador = documento.createElement("Torre");
            } else {
                nodoOrdenador = documento.createElement("Portatil");
            }

            raiz.appendChild(nodoOrdenador);


            /*-----------------SEPARADOR DE NODOS-------------------*/

            //Creo la etiqueta marca y la añado al nodoOrdenador.
            nodoDatos = documento.createElement("marca");
            nodoOrdenador.appendChild(nodoDatos);

            // Busco el dato que hay dentro del parametro marca y lo añado.
            texto = documento.createTextNode(unOrdenador.getMarca());
            nodoDatos.appendChild(texto);

            /*-----------------SEPARADOR DE NODOS-------------------*/

            //Creo la etiqueta modelo y la añado al nodoOrdenador.
            nodoDatos = documento.createElement("modelo");
            nodoOrdenador.appendChild(nodoDatos);

            // Busco el dato que hay dentro del parametro modelo y lo añado.
            texto = documento.createTextNode(unOrdenador.getModelo());
            nodoDatos.appendChild(texto);

            /*-----------------SEPARADOR DE NODOS-------------------*/

            //Creo la etiqueta procesador y la añado al nodoOrdenador.
            nodoDatos = documento.createElement("procesador");
            nodoOrdenador.appendChild(nodoDatos);

            // Busco el dato que hay dentro del parametro procesador y lo añado.
            texto = documento.createTextNode(unOrdenador.getProcesador());
            nodoDatos.appendChild(texto);

            /*-----------------SEPARADOR DE NODOS-------------------*/

            //Creo la etiqueta ghzProcesador y la añado al nodoOrdenador.
            nodoDatos = documento.createElement("ghz-procesador");
            nodoOrdenador.appendChild(nodoDatos);

            // Busco el dato que hay dentro del parametro ghzPorcesador y lo añado.
            texto = documento.createTextNode(String.valueOf(unOrdenador.getGhzProcesador()));
            nodoDatos.appendChild(texto);

            /*-----------------SEPARADOR DE NODOS-------------------*/

            //Creo la etiqueta ghzProcesador y la añado al nodoOrdenador.
            nodoDatos = documento.createElement("memoria-ram");
            nodoOrdenador.appendChild(nodoDatos);

            // Busco el dato que hay dentro del parametro memoriaRam y lo añado.
            texto = documento.createTextNode(unOrdenador.getMemoriaRam());
            nodoDatos.appendChild(texto);

            /*-----------------SEPARADOR DE NODOS-------------------*/

            //Creo la etiqueta tarjeta grafica y la añado al nodoOrdenador.
            nodoDatos = documento.createElement("tarjeta-grafica");
            nodoOrdenador.appendChild(nodoDatos);

            // Busco el dato que hay dentro del parametro tarjetaGrafica y lo añado.
            texto = documento.createTextNode(unOrdenador.getTarjetaGrafica());
            nodoDatos.appendChild(texto);

            /*-----------------SEPARADOR DE NODOS-------------------*/

            //Creo la etiqueta numSerie y la añado al nodoOrdenador.
            nodoDatos = documento.createElement("numero-serie");
            nodoOrdenador.appendChild(nodoDatos);

            // Busco el dato que hay dentro del parametro numeroSerie y lo añado.
            texto = documento.createTextNode(unOrdenador.getNumSerie());
            nodoDatos.appendChild(texto);

            /*-----------------SEPARADOR DE NODOS-------------------*/

            //Creo la etiqueta precio y la añado al nodoOrdenador.
            nodoDatos = documento.createElement("precio");
            nodoOrdenador.appendChild(nodoDatos);

            // Busco el dato que hay dentro del parametro precio y lo añado.
            texto = documento.createTextNode(String.valueOf(unOrdenador.getPrecio()));
            nodoDatos.appendChild(texto);

            /*-----------------SEPARADOR DE NODOS-------------------*/

            //Creo la etiqueta so y la añado al nodoOrdenador.
            nodoDatos = documento.createElement("sistema-operativo");
            nodoOrdenador.appendChild(nodoDatos);

            // Busco el dato que hay dentro del parametro so y lo añado.
            texto = documento.createTextNode(unOrdenador.getSo());
            nodoDatos.appendChild(texto);

            /*-----------------SEPARADOR DE NODOS-------------------*/

            //Creo la etiqueta fecha-alta y la añado al nodoOrdenador.
            nodoDatos = documento.createElement("fecha-alta");
            nodoOrdenador.appendChild(nodoDatos);

            // Busco el dato que hay dentro del parametro so y lo añado.
            texto = documento.createTextNode(unOrdenador.getFechaAlta().toString());
            nodoDatos.appendChild(texto);


            if (unOrdenador instanceof Torre) {
                /*-----------------SEPARADOR DE NODOS-------------------*/

                //Creo la etiqueta placa-base y la añado al nodoOrdenador.
                nodoDatos = documento.createElement("placa-base");
                nodoOrdenador.appendChild(nodoDatos);

                // Busco el dato que hay dentro del parametro placa base que se encuentra en la clase Torre  y lo añado.
                texto = documento.createTextNode(((Torre) unOrdenador).getPlacaBase());
                nodoDatos.appendChild(texto);


            } else {
                /*-----------------SEPARADOR DE NODOS-------------------*/

                //Creo la etiqueta resolucion y la añado al nodoOrdenador.
                nodoDatos = documento.createElement("resolucion");
                nodoOrdenador.appendChild(nodoDatos);

                // Busco el dato que hay dentro del parametro resolucionPantalla que se encuentra en la clase Portatil  y lo añado.
                texto = documento.createTextNode(((Portatil) unOrdenador).getResPantalla());
                nodoDatos.appendChild(texto);

            }
        }
        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);
    }

    /**
     * Metodo para importar los datos de un fichero.
     * @param fichero recibe un parametro del tipo File
     * @throws ParserConfigurationException controla que las partes susceptibles a fallos no terminen la ejecucion del programa.
     * @throws IOException controla los posibles fallos que puedan surgir al importar un archivo.
     * @throws SAXException controla excepciones que puedan surgir con las extensiones XML.
     */
    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaOrdenadores = new ArrayList<Ordenador>();
        Torre nuevaTorre = null;
        Portatil nuevoPortatil = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoOrdenador = (Element) listaElementos.item(i);
            if (nodoOrdenador.getTagName().equals("Torre")) {
                nuevaTorre = new Torre();
                nuevaTorre.setMarca(nodoOrdenador.getChildNodes().item(0).getTextContent());
                nuevaTorre.setModelo(nodoOrdenador.getChildNodes().item(1).getTextContent());
                nuevaTorre.setProcesador(nodoOrdenador.getChildNodes().item(2).getTextContent());
                nuevaTorre.setGhzProcesador(Double.parseDouble(nodoOrdenador.getChildNodes().item(3).getTextContent()));
                nuevaTorre.setMemoriaRam(nodoOrdenador.getChildNodes().item(4).getTextContent());
                nuevaTorre.setTarjetaGrafica(nodoOrdenador.getChildNodes().item(5).getTextContent());
                nuevaTorre.setNumSerie(nodoOrdenador.getChildNodes().item(6).getTextContent());
                nuevaTorre.setPrecio(Float.parseFloat(nodoOrdenador.getChildNodes().item(7).getTextContent()));
                nuevaTorre.setSo(nodoOrdenador.getChildNodes().item(8).getTextContent());
                nuevaTorre.setFechaAlta(LocalDate.parse(nodoOrdenador.getChildNodes().item(9).getTextContent()));
                nuevaTorre.setPlacaBase(nodoOrdenador.getChildNodes().item(10).getTextContent());

                listaOrdenadores.add(nuevaTorre);
            } else {
                if (nodoOrdenador.getTagName().equals("Portatil")) {
                    nuevoPortatil = new Portatil();
                    nuevoPortatil.setMarca(nodoOrdenador.getChildNodes().item(0).getTextContent());
                    nuevoPortatil.setModelo(nodoOrdenador.getChildNodes().item(1).getTextContent());
                    nuevoPortatil.setProcesador(nodoOrdenador.getChildNodes().item(2).getTextContent());
                    nuevoPortatil.setGhzProcesador(Double.parseDouble(nodoOrdenador.getChildNodes().item(3).getTextContent()));
                    nuevoPortatil.setMemoriaRam(nodoOrdenador.getChildNodes().item(4).getTextContent());
                    nuevoPortatil.setTarjetaGrafica(nodoOrdenador.getChildNodes().item(5).getTextContent());
                    nuevoPortatil.setNumSerie(nodoOrdenador.getChildNodes().item(6).getTextContent());
                    nuevoPortatil.setPrecio(Float.parseFloat(nodoOrdenador.getChildNodes().item(7).getTextContent()));
                    nuevoPortatil.setSo(nodoOrdenador.getChildNodes().item(8).getTextContent());
                    nuevoPortatil.setFechaAlta(LocalDate.parse(nodoOrdenador.getChildNodes().item(9).getTextContent()));
                    nuevoPortatil.setResPantalla(nodoOrdenador.getChildNodes().item(10).getTextContent());

                    listaOrdenadores.add(nuevoPortatil);
                }
            }
        }

    }
}