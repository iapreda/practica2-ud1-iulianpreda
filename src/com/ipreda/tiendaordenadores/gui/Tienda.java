package com.ipreda.tiendaordenadores.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.ipreda.tiendaordenadores.base.Ordenador;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Clase creada de forma automatica al crear el formulario de tienda.
 * Todos los atributos son publicos para que se puedan utilizar desde otras clases sin necesidad de getters ni setters.
 * El atributo private JPanel es privado porque sólo se necesita en esta clase.
 */
public class Tienda {
    private JPanel panel1;
    public JFrame frame;
    public JTextField txMarca;
    public JTextField txModelo;
    public JTextField txProcesador;
    public JTextField txGHz;
    public JTextField txPlacaBase;
    public JTextField txGrafica;
    public JTextField txNumSerie;
    public JTextField txPrecio;
    public JButton bAltaPC;
    public JButton bExportar;
    public JButton bImportar;
    public JButton bSalir;
    public JRadioButton rbTorre;
    public JRadioButton rbOrdenadorPortatil;
    public DatePicker dPickerFechaAlta;
    public JTextField txSO;
    public JList list1;
    public JTextField txRam;
    public JTextField txResPantalla;


    // Elemento creado por mi.
    public DefaultListModel<Ordenador> dlmOrdenador;


    /**
     * Constructor de la clase tienda.
     * En este consutrctor defino el marco que llevará todos los campos creados en el formulario.
     */
    public Tienda() {
        frame = new JFrame("YULI PC-SHOP MVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        initComponents();
    }

    /**
     * Con este metodo inicializo la lista.
     */
    private void initComponents() {
        dlmOrdenador = new DefaultListModel<Ordenador>();
        list1.setModel(dlmOrdenador);
    }


 }
