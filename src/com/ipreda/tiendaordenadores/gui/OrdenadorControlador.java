package com.ipreda.tiendaordenadores.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.ipreda.tiendaordenadores.base.Ordenador;
import com.ipreda.tiendaordenadores.base.Portatil;
import com.ipreda.tiendaordenadores.base.Torre;
import com.ipreda.tiendaordenadores.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Properties;

public class OrdenadorControlador implements ActionListener, ListSelectionListener, WindowListener {

    private Tienda vista;
    private OrdenadorModelo modelo;
    private File ultimaRutaExportada;


    /**
     * Constructor de la clase OrdenadorControlador.
     * Este constructor cuenta con un tryCatch para prevenir fallos y que la ejecucion del programa no termine.
     *
     * @param vista  recibe parametros de la clase Tienda.
     * @param modelo recibe parametros de la clase OrdenadorModelo.
     * @see Tienda
     * @see OrdenadorModelo
     */
    public OrdenadorControlador(Tienda vista, OrdenadorModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;


        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }

        addActionListener(this);
        addListSelecionListener(this);
        addWindowListener(this);
    }


    /**
     * Metodo generado de forma automatica por los imports.
     * En esta clase se encuentra lo siguiente:
     * Un if else que controla los radioButton. En funcion de la selección del radioButton se habilita una textField para escribir.
     * Si se selecciona el radioButton de Torre, se habilita el textField de placa base y se deshabilita la escritura de resolucion Pantalla.
     * Si se selecciona el radioButton de ordenador portatil, el textfield de placabase se deshabilita y se habilita el textField de resolucion pantalla.
     * El motivo de porque se ha hecho esto es el siguiente:
     * si quiero rellenar un ordenador de torre, el campo unico del ordenador portatil no me interesa rellenarlo, y viceversa.
     * Despues del if está el switch case del boton de ALTA, donde primero se comprueba si hay campos vacíos o no.
     * Despues de la comprobacion de los campos vacios, se comprueba el numero de serie, cuyo campo es unico para el programa. Si previamente se ha dado de alta un
     * ordenador con el mismo numero de serie, saltara un aviso.
     * Si no hay ningun campo vacio ni error, se entra en los correspondientes Ifs.
     * Por ultimo, en el case "ALTA" los campos se vacian para proceder a introducir otro ordenador.
     *
     * Case exportar: se enlaza con otros metodos para exportar el fichero a extension xml.
     * Case importar: se enlaza con otros metodos para importar el fichero exportado previamente. Despues se refresca la lista para mostrar los elementos introducidos.
     * Case Salir del programa: Finaliza la ejecucion.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        if (vista.rbTorre.isSelected()) {
            vista.txResPantalla.setEnabled(false);
            vista.txPlacaBase.setEnabled(true);
        } else {
            vista.txPlacaBase.setEnabled(false);
            vista.txResPantalla.setEnabled(true);
        }
        switch (actionCommand) {

            case "ALTA":
                if (camposVacios()) {
                    Util.errorMsg("Los siguientes campos no pueden estar vacios\n" +
                            "Marca\n Modelo\n Procesador\n GHzProcesador\n Memoria RAM\n Tarjeta Gráfica\n" +
                            "NºSerie\n Precio\n Fecha Alta\n");
                    break;
                }
                if (modelo.existeNumSerie(vista.txNumSerie.getText())) {
                    Util.errorMsg("Ya se ha dado de alta un ordenador con este numero de serie:\n " + vista.txNumSerie.getText());
                    break;

                }
                if (vista.rbTorre.isSelected()) {

                    modelo.altaTorre(vista.txMarca.getText(),
                            vista.txModelo.getText(),
                            vista.txProcesador.getText(),
                            Double.parseDouble(vista.txGHz.getText()),
                            vista.txRam.getText(), vista.txGrafica.getText(),
                            vista.txNumSerie.getText(),
                            Float.parseFloat(vista.txPrecio.getText()),
                            vista.txSO.getText(),
                            vista.dPickerFechaAlta.getDate(),
                            vista.txPlacaBase.getText());


                } else {
                    modelo.altaPortatil(vista.txMarca.getText(),
                            vista.txModelo.getText(),
                            vista.txProcesador.getText(),
                            Double.parseDouble(vista.txGHz.getText()),
                            vista.txRam.getText(), vista.txGrafica.getText(),
                            vista.txNumSerie.getText(),
                            Float.parseFloat(vista.txPrecio.getText()),
                            vista.txSO.getText(),
                            vista.dPickerFechaAlta.getDate(),
                            vista.txResPantalla.getText());

                }

                limpiarCampos();
                refrescar();
                break;
            case "EXPORTAR":

                JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivos XML", ".xml");
                int opt = selectorFichero.showSaveDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }

                break;

            case "IMPORTAR":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivos XML", ".xml");
                int opt2 = selectorFichero2.showOpenDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();

                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }

                break;

            case "SALIR DEL PROGRAMA":
                System.exit(0);
                break;
        }
    }


    /**
     * Metodo para comprobar si hay campos vacios.
     * @return devuelve "true" si hay campos vacios
     * @return devuelve "false" si no hay campos vacios.
     */
    private boolean camposVacios() {
        if (vista.txModelo.getText().isEmpty() ||
                vista.txModelo.getText().isEmpty() ||
                vista.txProcesador.getText().isEmpty() ||
                vista.txGHz.getText().isEmpty() ||
                vista.txRam.getText().isEmpty() ||
                vista.txGrafica.getText().isEmpty() ||
                vista.txNumSerie.getText().isEmpty() ||
                vista.txPrecio.getText().isEmpty() ||
                vista.dPickerFechaAlta.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Metodo para vaciar los campos despues de haber introducido datos.
     */
    private void limpiarCampos() {
        vista.txMarca.setText(null);
        vista.txModelo.setText(null);
        vista.txProcesador.setText(null);
        vista.txGHz.setText(null);
        vista.txRam.setText(null);
        vista.txGrafica.setText(null);
        vista.txNumSerie.requestFocus();
        vista.txPrecio.setText(null);
        vista.txSO.setText(null);
        vista.dPickerFechaAlta.setText(null);
        vista.txPlacaBase.setText(null);
        vista.txResPantalla.setText(null);

    }

    /**
     * Metodo para actualizar los campos.
     */
    private void refrescar() {
        vista.dlmOrdenador.clear();
        for (Ordenador unOrdenador : modelo.obtenerOrdenadores()) {
            vista.dlmOrdenador.addElement(unOrdenador);
        }
    }

    /**
     * Metodo para actualizar el fichero configuracion
     * @throws IOException lanza una excepcion en caso de que se produzca un error con el fichero.
     */
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("ordenador.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    /**
     * Metodo para acutalizar el fichero .conf
     * @param ultimaRutaExportada
     */
    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    /**
     * Metodo para guardar la configuracion de la ultima ruta exportada. Se actualiza de forma automatica con la ruta del fichero.
     * @throws IOException lanza una excepcion en caso de que se produzca un error con el fichero.
     */
    private void guardarConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("ordenador.conf"), "Datos de configuracion de ordenadores.");
    }

    /**
     * Metodo para agregar los listener a los botones, y que estos hagan lo que se requiera en el mismo.
     * @param ordenadorControlador
     */
    private void addActionListener(ActionListener ordenadorControlador) {
        vista.rbTorre.addActionListener(ordenadorControlador);
        vista.rbOrdenadorPortatil.addActionListener(ordenadorControlador);
        vista.bAltaPC.addActionListener(ordenadorControlador);
        vista.bExportar.addActionListener(ordenadorControlador);
        vista.bImportar.addActionListener(ordenadorControlador);
        vista.bSalir.addActionListener(ordenadorControlador);


    }

    /**
     * Metodo para agrear un listener a la ventana.
     * @param ordenadorControlador
     */
    private void addWindowListener(WindowListener ordenadorControlador) {
        vista.frame.addWindowListener(ordenadorControlador);
    }

    /**
     * Metodo para agregar un listener a la lista.
     * @param ordenadorControlador
     */
    private void addListSelecionListener(ListSelectionListener ordenadorControlador) {

        vista.list1.addListSelectionListener(ordenadorControlador);

    }


    @Override
    public void windowOpened(WindowEvent e) {

    }

    /**
     * Metodo para "el evento" de cuando se cierra la ventana.
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {

        int resp = Util.mensajeConfirmacion("¿Deseas finalizar la ejecución?", "Salir");
        if (resp == JOptionPane.YES_OPTION) {
            try {
                guardarConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);

        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }


    /**
     * Metodo que se utiliza cuando se cambia la seleccion de componentes.
     * Notifica cuando un valor de la lista ha cambiado.
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting()) {
            Ordenador ordenadorSeleccionado = (Ordenador) vista.list1.getSelectedValue();
            vista.txMarca.setText(ordenadorSeleccionado.getMarca());
            vista.txModelo.setText(ordenadorSeleccionado.getMarca());
            vista.txProcesador.setText(ordenadorSeleccionado.getProcesador());
            vista.txGHz.setText(String.valueOf(ordenadorSeleccionado.getGhzProcesador()));
            vista.txGrafica.setText(ordenadorSeleccionado.getTarjetaGrafica());
            vista.txNumSerie.setText(ordenadorSeleccionado.getNumSerie());
            vista.txPrecio.setText(String.valueOf(ordenadorSeleccionado.getPrecio()));
            vista.txSO.setText(ordenadorSeleccionado.getSo());
            vista.dPickerFechaAlta.setDate(ordenadorSeleccionado.getFechaAlta());
            if (ordenadorSeleccionado instanceof Torre) {
                vista.rbTorre.doClick();
                vista.txPlacaBase.setText(String.valueOf(((Torre) ordenadorSeleccionado).getPlacaBase()));
            } else if (ordenadorSeleccionado instanceof Portatil) {
                vista.rbOrdenadorPortatil.doClick();
                vista.txResPantalla.setText(((Portatil) ordenadorSeleccionado).getResPantalla());

            }
        }

    }
}
