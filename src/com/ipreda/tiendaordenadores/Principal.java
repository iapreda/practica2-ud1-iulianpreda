package com.ipreda.tiendaordenadores;

import com.ipreda.tiendaordenadores.gui.OrdenadorControlador;
import com.ipreda.tiendaordenadores.gui.OrdenadorModelo;
import com.ipreda.tiendaordenadores.gui.Tienda;

public class Principal {
    /**
     * Clase principal que da acceso a funcionalidad del programa
     * @param args
     */
    public static void main(String[] args) {
        Tienda vista = new Tienda();
        OrdenadorModelo modelo = new OrdenadorModelo();
        OrdenadorControlador cotnrolador = new OrdenadorControlador(vista,modelo);
    }



}
